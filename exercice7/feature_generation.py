import pandas as pd
import numpy as np

gamma = pd.read_csv("Gamma-MC.csv", sep = " ")
proton = pd.read_csv("Proton-MC.csv", sep = " ")

gamma.insert(0, "label", ["Gamma"]*len(gamma))
proton.insert(0, "label", ["Proton"]*len(proton))
testdaten = pd.read_csv("Test-Daten.csv", sep = " ")

training = gamma.append(proton, ignore_index=True)

label = training.iloc[:,0]
training = training.drop(training.columns[[0,1]], axis=1)

ID = testdaten.iloc[:,0]
testdaten = testdaten.drop(testdaten.columns[[0]], axis=1)

training_norm = (training - training.min())
training_norm = training_norm / training_norm.max()

testdaten_norm = (testdaten - testdaten.min())
testdate_norm = testdaten_norm / testdaten_norm.max()



### Nur Attribute mit Varianz gross genug werden ausgewaehlt.
var = np.zeros(35)
liste = []
for i in np.arange(0,35):
	var[i]= np.var(training_norm.iloc[:,i])
	if var[i] > 0.02:
		liste = np.append(liste,i)

names = list(training_norm.columns.values)
names2 = list(testdaten_norm.columns.values)
print(names)
print(names2)

liste = liste.astype(int)
liste2 = liste
for i in liste:
	liste2 = np.delete(liste2,0)
	for j in liste2:
		training_norm[names[i]+"-"+names[j]] = training_norm.iloc[:,i]-training_norm.iloc[:,j]
		training_norm[names[i]+"*"+names[j]] = training_norm.iloc[:,i]*training_norm.iloc[:,j]
		training_norm[names[i]+"/"+names[j]] = training_norm.iloc[:,i]/training_norm.iloc[:,j]
		training_norm["ratio("+names[i]+","+names[j]+")"] = (training_norm.iloc[:,i]-training_norm.iloc[:,j])/(training_norm.iloc[:,i]+training_norm.iloc[:,j])
		
		testdaten_norm[names2[i]+"-"+names2[j]] = testdaten_norm.iloc[:,i]-testdaten_norm.iloc[:,j]
		testdaten_norm[names2[i]+"*"+names2[j]] = testdaten_norm.iloc[:,i]*testdaten_norm.iloc[:,j]
		testdaten_norm[names2[i]+"/"+names2[j]] = testdaten_norm.iloc[:,i]/testdaten_norm.iloc[:,j]
		testdaten_norm["ratio("+names[i]+","+names[j]+")"] = (testdaten_norm.iloc[:,i]-testdaten_norm.iloc[:,j])/(testdaten_norm.iloc[:,i]+testdaten_norm.iloc[:,j])



training_norm = pd.concat([label,training_norm],axis=1)

testdaten_norm = pd.concat([ID,testdaten_norm],axis=1)


training_norm.to_csv("training_normiert_var.csv", sep = ";", index = False)
testdaten_norm.to_csv("training_normiert_var.csv", sep = ";", index = False)

