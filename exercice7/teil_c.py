import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

a = pd.read_csv("a_validation.csv", sep = ";",usecols=[1,2,4])
bins = 30
#Anlegen der notwenidgen Arrays
reinheit = np.zeros(bins)
effizienz = np.zeros(bins)
schnitt = np.zeros(bins)

for i in np.arange(0,bins):
	schnitt[i] = 0.5 + (0.5/(bins-1)) * i
	geschnitten = a[a["confidence(Gamma)"] >= schnitt[i]]
	true = geschnitten[geschnitten["label"] == geschnitten["prediction(label)"]]
	false = geschnitten[geschnitten["label"] != geschnitten["prediction(label)"]]
	#Definitionen aus der Vorlesung
	tp = len(true[true["label"] == "Gamma"])
	tn = len(true[true["label"] == "Proton"])
	fp = len(false[false["prediction(label)"] == "Gamma"])
	fn = len(false[false["prediction(label)"] == "Proton"])
	#Berechnung der Reinheit
	if tp + fp != 0:
		reinheit[i] = float(tp)/(float(tp)+float(fp))
	else:
		reinheit[i] = 0.
	#Berechnung der Effizienz
	effizienz[i] = float(tp)/(30000)
#beta-Wert ist festgelegt
beta = 1./6.
#Berechnung des F-Scores gemäß der Vorlesung
FScore = (1. + beta**2) * (reinheit * effizienz)/(beta**2 * reinheit + effizienz)

plt.bar(schnitt,FScore, width=0.5/(bins-1))
plt.ylabel('FScore')
plt.xlabel('Konfidenzschnitte')
plt.savefig('Blatt7_c1.pdf')
plt.show()