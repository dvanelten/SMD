import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import uncertainties.unumpy as unp

#n = number of validations
n = 10
parts = int((60000+n)/n)

per = {}
for i in range(0,n):
	per[i] = pd.read_csv("/Users/christopher/Dropbox/SMD/Code/Blatt7-Challlenge/ergebnisse/feature_generation.csv", sep = ";",usecols=[0,1,3],skiprows=parts*i, nrows=parts-1)

reinheit = {}
effizienz = {}
for k in range(n):
	bins = 21
	reinheit[k] = np.zeros(bins)
	effizienz[k] = np.zeros(bins)
	cut = np.zeros(bins)

	for i in np.arange(0,bins):
		cut[i] = 0.5 + (0.5/(bins-1)) * i
		cuted = per[k][per[k]["confidence(Gamma)"] >= cut[i]]

		true = cuted[cuted["label"] == cuted["prediction(label)"]]
		false = cuted[cuted["label"] != cuted["prediction(label)"]]

		tp = len(true[true["label"] == "Gamma"])
		tn = len(true[true["label"] == "Proton"])
		fp = len(false[false["prediction(label)"] == "Gamma"])
		fn = len(false[false["prediction(label)"] == "Proton"])
		
		if tp + fp != 0:
			reinheit[k][i] = float(tp)/(float(tp)+float(fp))
		else:
			reinheit[k][i] = 1.

		effizienz[k][i] = float(tp)/(3000)


reinheit_mittelwert = np.zeros(bins)
effizienz_mittelwert = np.zeros(bins)
for i in range(bins):
	for k in range(n):
		reinheit_mittelwert[i] += (reinheit[k][i] / n)
		effizienz_mittelwert[i] += (effizienz[k][i] / n)

reinheit_varianz = np.zeros(bins)
effizienz_varianz = np.zeros(bins)
for i in range(bins):
	for k in range(n):
		reinheit_varianz[i] +=  (reinheit[k][i]-reinheit_mittelwert[i])**2
		effizienz_varianz[i] +=  (effizienz[k][i]-effizienz_mittelwert[i])**2

reinheit_fehler = np.sqrt( reinheit_varianz / (n*(n-1)) )
effizienz_fehler = np.sqrt( effizienz_varianz / (n*(n-1)) )



reinheit = unp.uarray(reinheit_mittelwert, reinheit_fehler)
effizienz = unp.uarray(effizienz_mittelwert, effizienz_fehler)

b = 1./6.
F = (1. + b**2) * (reinheit * effizienz)/(b**2 * reinheit + effizienz)

mittel_F = []
delta_F = []
for i in range(bins):
	mittel_F = np.append(mittel_F, F[i].n)
	delta_F = np.append(delta_F, F[i].s)


fig, ax = plt.subplots()

plt.errorbar(cut ,mittel_F, xerr=0.25/(bins-1),yerr=delta_F,fmt="o")
plt.ylim([0.2,1])
ax.set_ylabel('F-Score')
ax.set_xlabel('Konfidenzschnitt')
ax.set_title('F-Score in Abhaengigkeit der Konfidenzschnitte')
plt.savefig("/Users/christopher/Dropbox/SMD/Code/Blatt7-Challlenge/Teil_e/Blatt7_e1.pdf")





