from __future__ import unicode_literals, print_function, division
import matplotlib.pyplot as plt

def aufg1():
    import numpy as np
    import ROOT as ROOT

    x = np.arange(1, 30, 0.001)

#Funktionen:
    y1 = 3 * x - 30 * np.log(x) + 45.96		#Aufgabenteil (a)
    y2 = 3/20 * (x - 10)**2 + 6.88		#Aufgabenteil (d)

    plt.plot(x,y1,label='$- L_{ln} (\lambda)$')
    plt.plot(x,y2,label='$- L_{ln,Taylor} (\lambda)$')
    plt.grid()
    plt.xlabel('$\lambda$')
    plt.ylabel('$-L (\lambda)$')
    plt.title('$Likelihoodkurve$')
    plt.legend()
    plt.show()



if __name__ == '__main__':
	aufg1()







