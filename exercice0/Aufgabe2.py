from __future__ import unicode_literals, division, print_function
import ROOT
import numpy as np
import matplotlib.pyplot as plt

def aufg1(N=100):                               	#Funktion, die das Array baut

#Array importiert aus Aufgabe 1b

#Spalten definieren
    spalte0 = np.arange(N)+1 				#Ausgabe der Werte bis N -> N selber nicht, daher +1
    spalte1 = np.random.uniform(0,1,N)						
    spalte2 = np.random.uniform(0,10,N)
    spalte3 = np.random.uniform(20,30,N)
    spalte4 = np.random.normal(0,1,N)						
    spalte5 = np.random.normal(5,2,N)
    spalte6 = spalte0**2								
    spalte7 = np.cos(spalte0)

#Array auffüllen und Rückgabewert erhalten
    ZweiDimArray = np.array([spalte0,spalte1,spalte2,spalte3,spalte4,spalte5,spalte6,spalte7])
    return ZweiDimArray                           
    

def aufg2(ZweiDimArray):                               	# Funktion, die als Eingangswerte Array erhält
    N = ZweiDimArray.shape[1]                           # numpy.ndarray.shape[i] liefert Tupel (Liste endlich vieler Objekte)
    canvas = ROOT.TCanvas("canvas","canvas",800,600)    # Konstruktor
    canvas.SetGrid()                                    # Gitter

    graph1 = ROOT.TGraph(N, ZweiDimArray[0], ZweiDimArray[6])   	#Anzahl Datenpunkte, x-Koordinaten, y-Koordinaten
    graph2 = ROOT.TGraph(N, ZweiDimArray[0], ZweiDimArray[7])   

    graph1.SetMarkerStyle(20)                       #großer Kreis (ROOT Dokumentation)
    graph1.SetMarkerColor(2)                        #rot (s.o.)
    graph1.SetTitle("x^2")                          #Titel (auch in Legende so)

    graph2.SetMarkerStyle(21)                       #großes Rechteck (s.o.)
    graph2.SetMarkerColor(3)                        #gruen (s.o.)
    graph2.SetTitle("Zwei Graphen;#it{x};#it{y}")   #Titel + Achsenbeschriftung
    graph2.GetXaxis().SetDecimals()
    graph2.GetYaxis().SetDecimals()

    graph2.Draw("AP")					
    graph1.Draw("P")                               #A: um diesen Graphen werden Axen angepasst (muss zuerst kommen)
   
    legend = ROOT.TLegend(0.9,0.7,0.7,0.9)
    legend.AddEntry(graph1,"#it{x}^{2}","p")
    legend.AddEntry(graph2,"cos(#it{x})","p")
    legend.SetHeader("Legende")
    legend.Draw()

    canvas.Modified()
    canvas.Update()
    canvas.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe2/Aufgabe2.png")


if __name__ == "__main__":
    ZweiDimArray = aufg1()
    aufg2(ZweiDimArray)
