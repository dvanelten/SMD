import ROOT
import numpy as np

canvas = ROOT.TCanvas("canvas", "TH1F Beispiel", 800, 600)

#Histogramme definieren
hist1 = ROOT.TH1F("hist1", "Gauss-Verteilungen", 20, -10, 10)			#Anzahl Bins, Intervall
hist2 = ROOT.TH1F("hist2", "hist2", 20, -10, 10)

#TRandom3 verwenden zum Generieren von Zufallszahlen
random_generator = ROOT.TRandom3()

#Erstes Histogramm füllen und modifizieren
random_numbers1 = [random_generator.Gaus() for i in range(1000)]
for random_number in random_numbers1:
    hist1.Fill(random_number)
hist1.SetLineColor(2)								#ROOT Dokumentation 
hist1.SetLineWidth(3)								#s.o.
hist1.SetLineStyle(10)								#s.o.

#zu Aufgabenteil (c) mit Klasse TH1
hist1.GetMean()
hist1.GetRMS()


#Zweites Histogramm füllen und modifizieren
random_numbers2 = [random_generator.Gaus(1,2) for i in range(1000)]
for random_number in random_numbers2:
    hist2.Fill(random_number)
hist2.SetLineColor(7)								
hist2.SetLineWidth(9)								
hist2.SetLineStyle(6)								

#mit Klasse TH1 Mean und RMS erhalten (wird automatisch ermittelt)
hist2.GetMean()
hist2.GetRMS()

#Histogramm2 skalieren
hist2.Scale(2)

#Bin auf Wert setzen
hist2.SetBinContent(14,0)							#Bin, Inhalt (zusaetzlicher Eintrag in Static Box)

#Static Box festlegen - Aufgabenteil (f)
ROOT.gStyle.SetOptStat("neRMiou")						#n=Name,e=Entries,R=Fehler RMS,M=Fehler Mean,I=Integral
										#o=overflow,u=underflow / Großbuchstabe = Fehler

hist1.Draw()
hist2.Draw("SAMES")								#SAME: ins selbe Histogramm; SAMES: + Aktualisierung Static Box

#Einstellungen, damit beide
ROOT.gPad.Update()								#Update des Canvas nach Modifikation
stats2 = hist2.GetListOfFunctions().FindObject("stats")				#Statistische Eigenschaften von hist2
stats2.SetY1NDC(0.5)								
stats2.SetY2NDC(0.1)
stats2.Draw()



#Legende anlegen
legend = ROOT.TLegend(0.1,0.9,0.3,0.8)
legend.AddEntry(hist1, "Gauss1 mit #mu=0, #sigma=1")
legend.AddEntry(hist2, "Gauss1 mit #mu=1, #sigma=2")
legend.Draw()


canvas.Modified()
canvas.Update()
canvas.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe3/Aufgabe3.png")
