import ROOT
import numpy as np

#TFile und TTree erzeugen
root_file = ROOT.TFile("aufgabe4.root", "RECREATE")		#RECREATE: ueberschreibt ggf. existierende Datei
tree = ROOT.TTree("Zufallszahlen", "Zufallszahlen")

#Variablen anlegen
x = np.zeros(1, dtype=float)					#Dimension, Datentyp
y = np.zeros(1, dtype=float)
z = np.zeros(1, dtype=float)

#Branches hinzufügen
tree.Branch("x", x, "x/D")
tree.Branch("y", y, "y/D")
tree.Branch("z", z, "z/D")

#Tree füllen
generator = ROOT.TRandom3(0)
for i in range(1000):
    x[0] = generator.Rndm()*1000
    y[0] = generator.Gaus(x[0],x[0])
    z[0] = generator.Poisson(x[0])
    tree.Fill()

#Überprüfung, ob alles im Baum angekommen ist
#print(tree.GetBranch("x").GetEntries())			#Ausgabe, wenn alles geklappt hat: 1000

#File schreiben und schließen
root_file.Write()
root_file.Close()

#Tree wieder einlesen
readFile = ROOT.TFile("aufgabe4.root", "OPEN")
readTree = readFile.Get("Zufallszahlen")

#Eintraege im Baum ausgeben
#readTree.Scan("*")

#x in 1dim Hisogramm (b)
canvasb = ROOT.TCanvas("canvasb", "b", 800, 600)
readTree.Draw("x")
canvasb.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe4/aufgabe4b.png")

#x gegen y in 2dim Histogramm mittels COLZ (c)
canvasc = ROOT.TCanvas("canvasc", "c", 800,600)
readTree.Draw("y:x", "", "COLZ")
canvasc.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe4/aufgabe4c.png")

# x-z in 1dim Histogramm (d)
canvasd = ROOT.TCanvas("canvasd", "d", 800, 600)
readTree.Draw("(x-z)")
canvasd.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe4/aufgabe4d.png")

#x, bei denen y>50 (e)
canvase = ROOT.TCanvas("canvase", "e", 800, 600)
histe = ROOT.TH1F("histe", "e", 100, -100, 1100)
readTree.Project("histe", "x", "y > 50")
histe.Draw()
canvase.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe4/aufgabe4e.png")

#x in grün, y in schwarz in zwei 1dim Histogrammen (f)
canvasf = ROOT.TCanvas("canvasf", "f", 800, 600)

histfb = ROOT.TH1F("histfb", "fblack", 100, -1000, 2000)
histfb.SetLineColor(1) 							#schwarz
readTree.Project("histfb", "y")
histfb.Draw()

histfg = ROOT.TH1F("histfg", "fgreen", 100, -100, 1100)
readTree.Project("histfg", "x")
histfg.SetLineColor(8) 							#gruen
histfg.Draw("same")
canvasf.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe4/aufgabe4f.png")

#x und z in zwei 1dim Histogrammen nebeneinander (g)
canvasg = ROOT.TCanvas("canvasg", "g", 800, 600)
canvasg.Divide(2,1)							#Canvas unterteilen

#nutze das Histogramm aus (f) für x
canvasg.cd(1)
histfg.Draw()

canvasg.cd(2)
histg = ROOT.TH1F("histg", "g", 100, -100, 1100)
readTree.Project("histg", "z")
histg.Draw()
canvasg.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe4/aufgabe4g.png")

#x in 1dim Histogramm mit 10 Bins zwischen 0 und 100 (h)
canvash = ROOT.TCanvas("canvash", "h", 800,600)
histh = ROOT.TH1F("histh", "h", 10, 0, 100)
readTree.Project("histh", "x", "x < 101")
histh.Draw()
canvash.SaveAs("/home/dvanelten/SMD/Aufgaben/Blatt0/Aufgabe4/aufgabe4h.png")
