from __future__ import print_function, unicode_literals, division

def aufg3():
	import matplotlib.pyplot as plt
	import numpy as np
	import ROOT
	import root_numpy as rnp


	#Numerische Bestimmung und Scatter-Plot - Aufgabenteil (b)
	data = np.transpose(np.random.multivariate_normal([1.0,1.0], [[0.04,-0.032],[-0.032,0.04]],10000))
	plt.plot(data[0],data[1],'.')
	plt.xlabel("$a_1$")
	plt.ylabel("$a_2$")
	plt.show()


	#Vorhersage y für verschiedene x-Werte - Aufgabenteil (c)
	def f(x):
	    y = data[0] + x * (data[1])
	    return y

	x_werte = [-3,0,3]

	for x in x_werte:
	    print("x = %i:" %x)
	    y = f(x)
	    plt.hist(y,50)
	    plt.xlabel(r"$y$")
	    plt.show()
	    sigma_analytisch = np.sqrt(0.04*(1+x*x)-0.064*x)
	    sigma_numerisch = np.std(y)
	    delta = (sigma_numerisch-sigma_analytisch)/sigma_analytisch 	#rel. Fehler

	    #Vergleich der beiden Unsicherheiten:
	    print("Fehler Analytisch: " + str(sigma_analytisch))
	    print("Fehler Numerisch: " + str(sigma_numerisch))
	    print(r"Prozentuale Abweichung: %.2f" %(delta*100))
	    print(" ")


if __name__ == '__main__':
	aufg3()
