import ROOT
import numpy as np
import matplotlib.pyplot as plt
from root_numpy import root2array

def aufg1():

###########
#Aufgabenteil d)
###########

	def kNN(training,label,daten,k):
		abstand = np.empty(len(training[0]))
		erg = np.zeros(len(daten[0]))

		for i in range(len(daten[0])):
			for j in range(len(training[0])):
			#kosinusähnlichkeit
				#abstand[j] = np.dot(training[:,j],daten[:,i])/(np.linalg.norm(training[:,j])*np.linalg.norm(daten[:,i]))
			#euklidischer abstand
				abstand[j] = np.sqrt(sum(np.power((training[:,j]-daten[:,i]),2)))
			l = sum(label[np.argsort(abstand)][0:k])
			if l >= k/2:
				erg[i] = 1
			#print(i)

		return erg
		

###########
#Aufgabenteil e)
###########

#ROOT-file öffnen
	file_name = ROOT.TFile("NeutrinoMC.root",'OPEN')
#signal-ereignisse einlesen
	s = file_name.Get('signal_mc_akzeptanz')
	laenge1 = s.GetEntries()

	signal = np.empty((3,laenge1))
	signal[0] = root2array("NeutrinoMC.root",'signal_mc_akzeptanz')['AnzahlHits']
	signal[1] = root2array("NeutrinoMC.root",'signal_mc_akzeptanz')['x']
	signal[2] = root2array("NeutrinoMC.root",'signal_mc_akzeptanz')['y']
#untergrund-ereignisse einlesen	
	u = file_name.Get('untergrund_mc')
	laenge2 = u.GetEntries()

	untergrund = np.empty((3,laenge2))
	untergrund[0] = root2array("NeutrinoMC.root",'untergrund_mc')['AnzahlHits']
	untergrund[1] = root2array("NeutrinoMC.root",'untergrund_mc')['x']
	untergrund[2] = root2array("NeutrinoMC.root",'untergrund_mc')['y']

#erstellen der trainingsdaten, der testdaten und des lables der testdaten (1=signal, 0=untergrund)
	training = np.append(signal[:,0:5000],untergrund[:,0:5000],axis=1)
	daten = np.append(signal[:,5000:15000],untergrund[:,5000:25000],axis=1)
	tlabel = np.append(np.ones(5000),np.zeros(5000))
	k = 10
#kNN einwenden
	dlabel = kNN(training,tlabel,daten,k)


#effizienz, reinheit und signifikanz bestimmen
	fn = 0
	tp = 0
	fp = 0
	for i in range(10000):
		if dlabel[i] == 1 :
			tp = tp+1
		else :
			fn = fn+1
	for i in range(20000):
		if dlabel[i+10000] == 1:
			fp = fp+1

	if fn+tp == 0:
		eff = 0
		print("Division durch Null bei Effizienzbestimmung")
	else:
		eff = tp/(fn+tp)
		print(eff)

	if fp+tp == 0:
		rei = 0
		print("Division durch Null bei Reinheitsbestimmung")
	else:
		rei = tp/(fp+tp)
		print(rei)

	if tp+fp == 0:
		sign = 0
		print("Division durch Null bei Signifikanzbestimmung")
	else:
		sign = tp/(np.sqrt(tp+fp))
		print(sign)
	
###########
#Aufgabenteil f)
###########	
#log10 von AnzahlHits verwenden
	signal2=np.empty((len(signal[:,0]),len(signal[0,:])))
	signal2[0] = np.log10(signal[0])
	signal2[1] = signal[1]
	signal2[2] = signal[2]
	untergrund2=np.empty((len(untergrund[:,0]),len(untergrund[0,:])))
	untergrund2[0] = np.log10(untergrund[0])	
	untergrund2[1] = untergrund[1]
	untergrund2[2] = untergrund[2]

#erstellen der trainingsdaten, der testdaten und des lables der testdaten (1=signal, 0=untergrund)
	training2 = np.append(signal2[:,0:5000],untergrund2[:,0:5000],axis=1)
	daten2 = np.append(signal2[:,5000:15000],untergrund2[:,5000:25000],axis=1)
	tlabel2 = np.append(np.ones(5000),np.zeros(5000))
	k2 = 10
#kNN einwenden
	dlabel2 = kNN(training2,tlabel2,daten2,k2)


#effizienz, reinheit und signifikanz bestimmen
	fn2 = 0
	tp2 = 0
	fp2 = 0
	for i in range(10000):
		if dlabel2[i] == 1 :
			tp2 = tp2+1
		else :
			fn2 = fn2+1
	for i in range(20000):
		if dlabel2[i+10000] == 1:
			fp2 = fp2+1

	if fn2+tp2 == 0:
		eff2 = 0
		print("Division durch Null bei Effizienzbestimmung")
	else:
		eff2 = tp2/(fn2+tp2)
		print(eff2)

	if fp2+tp2 == 0:
		rei2 = 0
		print("Division durch Null bei Reinheitsbestimmung")
	else:
		rei2 = tp/(fp2+tp2)
		print(rei2)

	if tp2+fp2 == 0:
		sign2 = 0
		print("Division durch Null bei Signifikanzbestimmung")
	else:
		sign2 = tp2/(np.sqrt(tp2+fp2))
		print(sign2)

	
###########
#Aufgabenteil g)
###########

#k=20
	k3 = 20

#kNN einwenden
	dlabel3 = kNN(training,tlabel,daten,k3)


#effizienz, reinheit und signifikanz bestimmen
	fn3 = 0
	tp3 = 0
	fp3 = 0
	for i in range(10000):
		if dlabel3[i] == 1 :
			tp3 = tp3+1
		else :
			fn3 = fn3+1
	for i in range(20000):
		if dlabel3[i+10000] == 1:
			fp3 = fp3+1


	if fn3+tp3 == 0:
		eff3 = 0
		print("Division durch Null bei Effizienzbestimmung")
	else:
		eff3 = tp3/(fn3+tp3)
		print(eff3)

	if tp3+fp3 == 0:
		rei3 = 0
		print("Division durch Null bei Reinheitsbestimmung")
	else:
		rei3 = tp/(fp3+tp3)
		print(rei3)

	if tp3+fp3 == 0:
		sign3 = 0
		print("Division durch Null bei Signifikanzbestimmung")
	else:
		sign3 = tp3/(np.sqrt(tp3+fp3))
		print(sign3)

if __name__ == '__main__':
	aufg1()