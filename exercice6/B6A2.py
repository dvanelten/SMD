from __future__ import print_function, unicode_literals, division

def aufg2():
	import numpy as np
	import math as m
	import matplotlib.pyplot as plt


	#Werte des Datensatzes als Arrays
	temp = [29.4, 26.7,28.3,21.1,20.0,18.3,17.8,22.2,20.6,23.9,23.9,22.2,27.2,21.7]
	wetter = [2,2,1,0,0,0,1,2,2,0,2,1,1,0]
	luft = [85,90,78,96,80,70,65,95,70,80,70,90,75,80]
	wind = [0,1,0,0,0,1,1,0,0,0,1,1,0,1]
	fussball = [0,0,1,1,1,0,1,0,1,1,1,1,1,0]

	#Entropie fuer binaere Entscheidung bestimmen
	def entropie(p,n):
		if(p == 0 or n == 0):
			e = 0
		else:
			e = -(p/(p+n))*m.log((p/(p+n)),2) - (n/(p+n))*m.log((n/(p+n)),2)
		return e


	#Definition Informationsgewinn und Zuordnung nach Schnitt
	#x2 entspricht hier dem Zielattribut: Fussball
	def gain(x1,x2, schnitt):
		n = len(x1)
		p1 = 0
		p2 = 0
		n1 = 0
		n2 = 0
		for i in range(n):
			if(x1[i] < schnitt):
				if(x2[i] == 1): 
					p1 += 1
				else: 
					n1 += 1
			else:
				if(x2[i] == 1): 
					p2 += 1
				else: 
					n2 += 1
		e = entropie(p1+p2,n1+n2)
		e1 = entropie(p1,n1)
		e2 = entropie(p2,n2)
		gain = e - ((p1+n1)/n)*e1 - ((p2+n2)/n)*e2
		return gain

	#Werte runden und Array fuer Plot vorbereiten
	tempstep = np.arange(m.floor(min(temp)), m.ceil(max(temp))+1)		#math.floor: Return the floor of x as a float, the largest integer value less than or equal to x
	tempgain = np.zeros(len(tempstep))									#math.ceil: Return the ceiling of x as a float, the smallest integer value greater than or equal to x
	for i in range(len(tempgain)):
		tempgain[i] = gain(temp, fussball, tempstep[i])

	wetterstep = np.arange(min(wetter), (max(wetter)+1))
	wettergain = np.zeros(len(wetterstep))
	for i in range(len(wettergain)):
		wettergain[i] = gain(wetter, fussball, wetterstep[i])

	luftstep = np.arange(m.floor(min(luft)), m.ceil(max(luft))+1)
	luftgain = np.zeros(len(luftstep))
	for i in range(len(luftgain)):
		luftgain[i] = gain(luft, fussball, luftstep[i])

	#Balkenbreite einstellen
	width = 0.2
	#Plots
	plt.bar(tempstep- 0.5*width, tempgain, width)						#plt.bar: Balkendiagramm
	plt.xlabel("Schnitt")
	plt.ylabel("Gain")
	plt.title("Informationsgewinn Temperatur")
	plt.savefig("temp.pdf")
	plt.clf()

	plt.bar(wetterstep- 0.5*width, wettergain, width)
	plt.xlabel("Schnitt")
	plt.ylabel("Gain")
	plt.title("Informationsgewinn Wettervorhersage")
	plt.savefig("wetter.pdf")
	plt.clf()

	plt.bar(luftstep- 0.5*width, luftgain, width)
	plt.xlabel("Schnitt")
	plt.ylabel("Gain")
	plt.title("Informationsgewinn Luftfeuchtigkeit")
	plt.savefig("luft.pdf")
	plt.clf()

	#wetter neu
	wetter_gain1 = entropie(9,5) - entropie(5,5)*(10/14) - entropie(4,0)*(4/14)
	x = np.array([0,1,2])
	g = [wettergain[1], wetter_gain1, wettergain[2]]
	plt.bar(x- 0.5*width, g, width)
	plt.xlabel("Element von $S_1$")
	plt.ylabel("Gain")
	plt.xlim(-0.5,2.5)
	plt.title("Informationsgewinn Wetter 2")
	plt.savefig("wetter2.pdf")
	plt.clf()


if __name__ == '__main__':
    aufg2()